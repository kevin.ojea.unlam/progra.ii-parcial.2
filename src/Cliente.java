import java.util.Calendar;
import java.util.Date;

public class Cliente {
    
    private String nombre;
    private Integer movil;
    private Date fechaHoraLlegada;
    private Date fechaHoraAtencion;


    public Cliente (String nombre, Integer movil, Date fechaHoraLlegada, Date fechaHoraAtencion) {
        this.nombre = nombre;
        this.movil = movil;
        this.fechaHoraLlegada = fechaHoraLlegada;
        this.fechaHoraAtencion = fechaHoraAtencion;
    }

    public Date getFechaHoraLlegada() {
        return this.fechaHoraLlegada;
    }

    public Date getFechaHoraAtencion() {
        return this.fechaHoraAtencion;
    }
    

    public void setFechaHoraAtencion(Date date) {
        this.fechaHoraAtencion = date;
    }

}
