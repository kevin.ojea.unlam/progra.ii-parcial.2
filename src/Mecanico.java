import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Set;

import excepciones.*;

public class Mecanico {
    
    Set<Cliente> clientesEnEspera = new LinkedHashSet<Cliente>();
    Set<Cliente> clientesAtendidos = new LinkedHashSet<Cliente>();

    public void nuevoCliente(Cliente cliente) {
        clientesEnEspera.add(cliente);
    }

    public Cliente atenderCliente() throws SinClientesEsperando {
        if(this.clientesEnEspera.size() != 0) {
            Cliente clienteAtendido = clientesEnEspera.iterator().next();
            clientesEnEspera.remove(clienteAtendido);
            clienteAtendido.setFechaHoraAtencion(new Date());
            clientesAtendidos.add(clienteAtendido);
            return clienteAtendido;
        } else {
            throw new SinClientesEsperando();
        }
        
    }

    public Integer tiempoEsperaNoAtendidos() {
        new Reloj();
        Long sumatoriaTiempo = Long.valueOf(0);
        Integer contador = 0;

        for (Cliente cliente : clientesEnEspera) {
            Calendar cal = Calendar.getInstance();
            cal.setTime(cliente.getFechaHoraLlegada());
            sumatoriaTiempo += cal.getTimeInMillis();
            contador++;
		}

        Long tiempoCalculado = new Date(Reloj.ahora() - (sumatoriaTiempo/contador)).getTime();
        Long minutos = (tiempoCalculado/1000)/60;

        return minutos.intValue();
    }

    public Integer tiempoEsperaAtendidos() {

        //PARA SIMULAR QUE LA HORA DE AHORA ES +2HS
        Calendar testCal = Calendar.getInstance();
        testCal.setTime(new Date());
        testCal.add(Calendar.HOUR, 2);
        Long tiempoAhora = testCal.getTimeInMillis();


        new Reloj();
        Long sumatoriaTiempo = Long.valueOf(0);
        Integer contador = 0;

        for (Cliente cliente : clientesAtendidos) {
            Calendar cal = Calendar.getInstance();
            cal.setTime(cliente.getFechaHoraAtencion());
            sumatoriaTiempo += cal.getTimeInMillis();
            contador++;
		}

        Long tiempoCalculado = new Date( /*Reloj.ahora()*/ tiempoAhora - (sumatoriaTiempo/contador)).getTime();
        Long minutos = (tiempoCalculado/1000)/60;

        return minutos.intValue();
    }

    public Integer obtenerCantidadClientesEnEspera() {
        return clientesEnEspera.size();
    }
    
    public Integer obtenerCantidadClientesAtenidos() {
        return clientesAtendidos.size();
    }

}
