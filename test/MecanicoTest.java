import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.junit.*;
import excepciones.*;

public class MecanicoTest {

    @Test
    public void atenderCliente() throws SinClientesEsperando {
        Mecanico mecanico = new Mecanico();
        Cliente cliente = new Cliente("Walter", 1123321441, new Date(), null);
        Cliente cliente2 = new Cliente("Pedro", 1123321441, new Date(), null);
        mecanico.nuevoCliente(cliente);
        mecanico.nuevoCliente(cliente2);
        Cliente clienteAtendido = mecanico.atenderCliente();
        assertEquals(clienteAtendido, cliente);
    }

    @Test (expected = SinClientesEsperando.class)
    public void noHayClientesParaAtender() throws SinClientesEsperando {
        Mecanico mecanico = new Mecanico();
        mecanico.atenderCliente();
        assertFalse(true);
    }

    @Test
    public void medioTiempoClientesEnEspera() throws SinClientesEsperando {

        //PARA SIMULAR CLIENTES QUE LLEGARON HACE 2HS
        Calendar testCal = Calendar.getInstance();
        testCal.setTime(new Date());
        testCal.add(Calendar.HOUR, -2);
        Date tiempoLlegada = testCal.getTime();


        Mecanico mecanico = new Mecanico();
        Cliente cliente = new Cliente("Walter", 1123321441, tiempoLlegada, null);
        Cliente cliente2 = new Cliente("Pedro", 1123321441, tiempoLlegada, null);
        Cliente cliente3 = new Cliente("Julian", 1123321441, tiempoLlegada, null);
        Cliente cliente4 = new Cliente("Tito", 1123321441, tiempoLlegada, null);
        mecanico.nuevoCliente(cliente);
        mecanico.nuevoCliente(cliente2);
        mecanico.nuevoCliente(cliente3);
        mecanico.nuevoCliente(cliente4);

        mecanico.atenderCliente();
        mecanico.atenderCliente();

        Integer promedioMinutosClientesEsperando = mecanico.tiempoEsperaNoAtendidos();

        assertEquals(promedioMinutosClientesEsperando, (Integer)120);
    }

    @Test
    public void medioTiempoClientesAtendidos() throws SinClientesEsperando {
        Mecanico mecanico = new Mecanico();
        Cliente cliente = new Cliente("Walter", 1123321441, new Date(), null);
        Cliente cliente2 = new Cliente("Pedro", 1123321441, new Date(), null);
        Cliente cliente3 = new Cliente("Julian", 1123321441, new Date(), null);
        Cliente cliente4 = new Cliente("Tito", 1123321441, new Date(), null);
        mecanico.nuevoCliente(cliente);
        mecanico.nuevoCliente(cliente2);
        mecanico.nuevoCliente(cliente3);
        mecanico.nuevoCliente(cliente4);

        mecanico.atenderCliente();
        mecanico.atenderCliente();

        Integer promedioMinutosClientesYaAtendidos = mecanico.tiempoEsperaAtendidos();

        assertEquals(promedioMinutosClientesYaAtendidos, (Integer)120);
    }

    @Test
    public void obtenerCantidadClientesEnEspera() throws SinClientesEsperando {
        Mecanico mecanico = new Mecanico();

        Cliente cliente = new Cliente("Walter", 1123321441, new Date(), null);
        Cliente cliente2 = new Cliente("Pedro", 1123321441, new Date(), null);
        Cliente cliente3 = new Cliente("Julian", 1123321441, new Date(), null);
        Cliente cliente4 = new Cliente("Tito", 1123321441, new Date(), null);

        mecanico.nuevoCliente(cliente);
        mecanico.nuevoCliente(cliente2);
        mecanico.nuevoCliente(cliente3);
        mecanico.nuevoCliente(cliente4);

        mecanico.atenderCliente();

        Integer clientesEnEspera = mecanico.obtenerCantidadClientesEnEspera();

        assertEquals(clientesEnEspera, (Integer)3);
    }

    @Test
    public void obtenerCantidadClientesAtendidos() throws SinClientesEsperando {
        Mecanico mecanico = new Mecanico();

        Cliente cliente = new Cliente("Walter", 1123321441, new Date(), null);
        Cliente cliente2 = new Cliente("Pedro", 1123321441, new Date(), null);
        Cliente cliente3 = new Cliente("Julian", 1123321441, new Date(), null);
        Cliente cliente4 = new Cliente("Tito", 1123321441, new Date(), null);

        mecanico.nuevoCliente(cliente);
        mecanico.nuevoCliente(cliente2);
        mecanico.nuevoCliente(cliente3);
        mecanico.nuevoCliente(cliente4);

        mecanico.atenderCliente();

        Integer clientesAtendidos = mecanico.obtenerCantidadClientesAtenidos();

        assertEquals(clientesAtendidos, (Integer)1);
    }

}